﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConvoII.models
{
    public class Persona
    {
        public String Nombre { get; set; }
        public String Apellidos { get; set; }
        public String Alias { get; set; }
        public String Correo { get; set; }
        public String Contraseña { get; set; }


    }
}
